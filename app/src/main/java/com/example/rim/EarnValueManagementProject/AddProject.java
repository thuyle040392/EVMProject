package com.example.rim.EarnValueManagementProject;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class AddProject extends AppCompatActivity {

    private EditText lastnameEditText;
    private EditText firstnameEditText;
    private EditText originaldurationEditText;
    private EditText employedEditText;
    private EditText jobDescEditText;
    private EditText dobEditText;
    private static final String TAG = "AddProject";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final MyprojectHandler db=new MyprojectHandler(AddProject.this);
        setContentView(R.layout.activity_add_project);



        lastnameEditText = (EditText) findViewById(R.id.lastnameEditText);
        firstnameEditText = (EditText) findViewById(R.id.firstnameEditText);
        originaldurationEditText = (EditText) findViewById(R.id.originaldurationEditText);
        employedEditText = (EditText) findViewById(R.id.employedEditText);
        jobDescEditText = (EditText) findViewById(R.id.jobDescEditText);
        dobEditText = (EditText) findViewById(R.id.dobEditText);
        lastnameEditText.addTextChangedListener(onTextChangedListener());
        Button saveButton = (Button) findViewById(R.id.saveButton);




        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String projectonamo = firstnameEditText.getText().toString();
                try {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime((new SimpleDateFormat("dd/MM/yyyy")).parse(
                            dobEditText.getText().toString()));
                    double date1 = calendar.getTimeInMillis();

                    calendar.setTime((new SimpleDateFormat("dd/MM/yyyy")).parse(
                            employedEditText.getText().toString()));
                    double date2 = calendar.getTimeInMillis();

                    if ( projectonamo.toString().matches("")) {
                        new AlertDialog.Builder(AddProject.this )
                                .setMessage( "Missing Project Name" ).show();
                    } else {
                    /*  saveToDB();*/
                        db.addproject(new Project(firstnameEditText.getText().toString(),
                                lastnameEditText.getText().toString().replaceAll(",", ""),
                                dobEditText.getText().toString(),jobDescEditText.getText().toString(),
                                employedEditText.getText().toString()));
                        Toast.makeText(getBaseContext(), "New Project  " + firstnameEditText.getText().toString()+" successfuly added ", Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception e) {
                    Log.e(TAG, "Error", e);
                    Toast.makeText(getBaseContext(), "Wrong Or Missing Data ", Toast.LENGTH_LONG).show();

                    return;
                }
            }
        });
    }

    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {

                lastnameEditText.removeTextChangedListener(this);
                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,###,###,###");
                    String formattedString = formatter.format(longval);
                    lastnameEditText.setText(formattedString);
                    lastnameEditText.setSelection(lastnameEditText.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                lastnameEditText.addTextChangedListener(this);
            }
        };
    }
}

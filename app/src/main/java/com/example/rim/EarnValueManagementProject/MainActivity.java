package com.example.rim.EarnValueManagementProject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_main);

        Button  createproject=(Button) findViewById(R.id.secondActivityButton);
        Button updateProject =(Button) findViewById(R.id.firstActivityButton);
        Button earnValue= (Button) findViewById(R.id.thirdActivityButton);

        createproject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, com.example.rim.EarnValueManagementProject.AddProject.class));
            }
        });


        updateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, com.example.rim.EarnValueManagementProject.UpdateProjectDetails.class));
            }
        });
        earnValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, com.example.rim.EarnValueManagementProject.EarnedValue.class));
            }
        });




    }

}
